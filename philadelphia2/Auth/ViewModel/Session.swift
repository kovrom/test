//
//  Session.swift
//  philadelphia2
//
//  Created by Роман Ковайкин on 27.10.2020.
//


import SwiftUI
import Foundation
import Firebase
import Combine
import GoogleSignIn


class SessionStore : ObservableObject {
    var didChange = PassthroughSubject<SessionStore, Never>()
//    var isLoggedIn = false { di dSet { self.didChange.send(self) }}
    var session: User? { didSet { self.didChange.send(self) }}
    var handle: AuthStateDidChangeListenerHandle?
    
    init(session: User? = nil) {
        self.session = session
    }
    
    func listen () {
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            if let user = user {
                print("Got user \(user)")
                self.session = User(uid: user.uid, email: user.email)
            } else {
                self.session = nil
            }
        }
    }
    
    func signInWithGoogle () {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func signOut () -> Bool {
        do {
            try Auth.auth().signOut()
            return true
        } catch {
            return false
        }
    }
    
    // stop listening for auth changes
    func unbind () {
        if let handle = handle {
            Auth.auth().removeStateDidChangeListener(handle)
        }
    }
    
    deinit {
        unbind()
    }
    
    func signUp(email: String, password: String, handler: @escaping AuthDataResultCallback) {
        Auth.auth().createUser(withEmail: email, password: password, completion: handler)
    }
    
    func signIn(email: String, password: String, handler: @escaping AuthDataResultCallback) {
        Auth.auth().signIn(withEmail: email, password: password, completion: handler)
    }
}



struct User {
    var uid: String
    var email: String?

    
    init(uid: String, email: String?) {
        self.uid = uid
        self.email = email
    }
}



/**
 * SessionStore manages the firebase user session. It contains the current user. It
 * also provides functions for signing up, signing in, etc.
 */
