//
//  ContentView.swift
//  philadelphia2
//
//  Created by Роман Ковайкин on 23.10.2020.
//

import SwiftUI

struct ContentView : View {
    
//    @State private var isUserLoggedIn: Bool = UserDefaults.standard.bool(forKey: "IsUserLoggedIn")
    @State private var isUserLoggedIn: Bool = false
    
    var body: some View {
        
        Group {
            if (isUserLoggedIn) {
                NameOfItem()
            } else {
                AuthenticationScreen()
            }
        }
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
