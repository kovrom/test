//
//  RegistrationView.swift
//  philadelphia2
//
//  Created by Роман Ковайкин on 21.10.2020.
//

import SwiftUI
import FirebaseUI

struct SignUpView : View {
    
    @State var email: String = ""
    @State var password: String = ""
    @State var loading = false
    @State var error = false
    @State var doNeedShow = false
    
    @EnvironmentObject var session: SessionStore
    
    func signUp () {
        print("sign me up")
        loading = true
        error = false
        session.signUp(email: email, password: password) { (result, error) in
            self.loading = false
            if error != nil {
                print("\(error)")
                self.error = true
                self.doNeedShow = false
            } else {
                UserDefaults.standard.set(true, forKey: "IsUserLoggedIn")
                UserDefaults.standard.synchronize()
                self.doNeedShow = true
                self.email = ""
                self.password = ""
                
            }
        }
    }
    
    var body : some View {
        VStack {
            
            Text("Создать аккаунт")
                .font(.title)
                .padding(.horizontal)
            
            CustomInput(text: $email, name: "Email")
                .padding()
            
            VStack(alignment: .leading) {
                SecureField("Password", text: $password).modifier(InputModifier())
                Text("Пароль должен быть длиннее 6 символов").font(.footnote).foregroundColor(Color.gray)
            }.padding(.horizontal)
            
            if (error) {
                InlineAlert(
                    title: "Хмм... Не сработало",
                    subtitle: "Вы уверены, что у вас нет аккаунта?"
                ).padding([.horizontal, .top])
                
            }
            
            CustomButton(label: "Зарегистрироваться", action: signUp).disabled(loading)
                .padding()
            GoogleLoginView()
                .padding()
            
            Spacer()
                .fullScreenCover(isPresented: $doNeedShow, content: NameOfItem.init)
        }
        
    }
    
}
