//
//  ContentView.swift
//  philadelphia2
//
//  Created by Роман Ковайкин on 21.10.2020.
//

import SwiftUI
import FirebaseUI

struct SignInView : View {
    
    @State var email: String = ""
    @State var password: String = ""
    @State var loading = false
    @State var error = false
    @State var doNeedShow = false
    
    @EnvironmentObject var session: SessionStore
    
    func signIn () {
        loading = true
        error = false
        session.signIn(email: email, password: password) { (result, error) in
            self.loading = false
            if error != nil {
                self.error = true
                self.doNeedShow = false
            } else {
                UserDefaults.standard.set(true, forKey: "IsUserLoggedIn")
                UserDefaults.standard.synchronize()
                self.doNeedShow = true
                self.email = ""
                self.password = ""
            }
        }
    }
    
    
    var body: some View {
        
        VStack {
            Spacer()
            Group {
                Text("Вход").font(.title).padding(.bottom)
            }

            Spacer()
            
            Group {
                Divider()
                CustomInput(text: $email, name: "Email")
                    .padding()
                
                SecureField("Пароль", text: $password)
                    .modifier(InputModifier())
                    .padding([.leading, .trailing])
                
                
                if (error) {
                    InlineAlert(title: "Хмм... Что-то пошло не так.",subtitle: "Пожалуйста, проверьте Email и пароль")
                        .padding([.horizontal, .top])
                    
                }
                
                CustomButton(label: "Войти", action: signIn, loading: loading)
                .padding()
            }
            
            VStack {
                Divider()
                HStack(alignment: .center) {
                    Text("Нет Аккаунта?")
                        .font(.footnote)
                        .foregroundColor(.gray)
                    
                    NavigationLink(destination: SignUpView()) {
                        Text("Зарегистрироваться").font(.footnote)
                    }
                }
                .padding()
            }
            .fullScreenCover(isPresented: $doNeedShow, content: NameOfItem.init)
        }
    }
}

struct AuthenticationScreen : View {
    var body : some View {
        NavigationView {
            SignInView()
        }
    }
}


#if DEBUG
struct Authenticate_Previews : PreviewProvider {
    static var previews: some View {
        AuthenticationScreen()
    }
}
#endif

