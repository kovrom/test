//
//  GraphView.swift
//  philadelphia2
//
//  Created by Роман Ковайкин on 21.10.2020.
//

import SwiftUI
import SwiftUICharts
import Alamofire

struct GraphView: View {
    let viewModel = GraphViewModel()
    let addItemViewModel = AddItemViewModel()
    var name: String
    var price: String
    var currentPrice: String
    @State var currencyType: Currency
    @State var data: [String]
    
    var body: some View {
        if data.count > 0{
            VStack(alignment: .center, spacing: 30, content: {
                BarChartView(data: ChartData(values: [("2000", viewModel.convertString(elem: price)),
                                                      ("2005", viewModel.convertString(elem: data[0])),
                                                      ("2010", viewModel.convertString(elem: data[1])),
                                                      ("2015", viewModel.convertString(elem: data[2])),
                                                      ("2020", currentPrice == "0" ? viewModel.convertString(elem: data[3]) : viewModel.convertString(elem: currentPrice))]),
                             title: "Изменение цены",
                             form: ChartForm.extraLarge)
                    .padding(.top, -200)
                
                HStack(alignment: .top, spacing: 40, content: {
                    VStack(alignment: .leading, content: {
                        Text("Должен стоить сейчас:")
                            .font(.title2)
                            .multilineTextAlignment(.leading)
                            .frame(width: 150)
                            .padding(.leading, /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                        
                        Text(data[3])
                            .font(.title2)
                            .multilineTextAlignment(.leading)
                            .padding(.leading, /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                        Text("С учетом инфляции")
                            .foregroundColor(Color.init(CGColor(red: 0, green: 0, blue: 0, alpha: 0.5)))
                            .frame(width: 175)
                    })
                    
                    VStack(alignment: .leading, content: {
                        Text("Через\n5 лет:")
                            .font(.title2)
                            .multilineTextAlignment(.leading)
                        
                        Text(String(viewModel.calculateInflationInFiveYears(data: data, price: price)) + " " + addItemViewModel.getCurrency(currencyType: currencyType))
                            .font(.title2)
                            .multilineTextAlignment(.leading)
                        Text("С учетом инфляции")
                            .foregroundColor(Color.init(CGColor(red: 0, green: 0, blue: 0, alpha: 0.5)))
                            .frame(width: 170)
                    })
                    
                    
                })
            })
            
            .onAppear(perform: {
                viewModel.saveItemToDatabase(name: name, data: data, infInFiveYears: viewModel.calculateInflationInFiveYears(data: data, price: price), price: price, currentPrice: currentPrice)
            })
            .navigationBarTitle(Text(name), displayMode: .large)
        }else{
            ProgressView("Вычисляем динамику изменения цены")
        }
        
    }
    
    
}


struct GraphView_Previews: PreviewProvider {
    static var previews: some View {
        GraphView(name: "BigMac", price: "200", currentPrice: "200", currencyType: Currency.RUB, data: ["100", "130", "145", "160","200"])
    }
}
