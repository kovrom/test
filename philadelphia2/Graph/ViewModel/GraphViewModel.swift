//
//  GraphViewModel.swift
//  philadelphia2
//
//  Created by Роман Ковайкин on 23.10.2020.
//

import Foundation
import Firebase


class GraphViewModel{
    
    func saveItemToDatabase(name: String, data: [String], infInFiveYears: Int, price: String, currentPrice: String){
        let value: [AnyHashable: Any] = [
            "Цена в 2000": price,
            "Цена в 2005": data[0],
            "Цена в 2010": data[1],
            "Цена в 2015": data[2],
            "Цена в 2020": currentPrice == "0" ? data[3] : currentPrice,
            "Цена в 2025": infInFiveYears
        ]
        
        Database.database().reference().child("Items").child(name).updateChildValues(value)
    }
    
    func convertString(elem: String) -> Float{
        return Float(elem.filter(("01234567890.").contains))!
    }
    
    func calculateInflationInFiveYears(data: [String], price: String) -> Int{
        // берем среднюю динамику по инфляции
        // умножаем ее на 5 и прибавлем к текущей
        //инфляция с 2015 - 2020
        
        let currentInflatoin = convertString(elem: data[3]) / convertString(elem: data[2])
        //2010-2015
        let inf2 = convertString(elem: data[2]) / convertString(elem: data[1])
        //2005-2010
        let inf3 = convertString(elem: data[1]) / convertString(elem: data[0])
        //2000-2005
        let inf4 = convertString(elem: data[0]) / convertString(elem: price)
        
        let average: Float = (currentInflatoin + inf2 + inf3 + inf4) / 4.0
        
        return Int((currentInflatoin + average * 5) * Float(price)!)
        
    }
}
