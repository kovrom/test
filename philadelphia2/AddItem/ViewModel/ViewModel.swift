//
//  ViewModel.swift
//  philadelphia2
//
//  Created by Роман Ковайкин on 23.10.2020.
//

import Foundation

class AddItemViewModel{
    
    func getCurrency(currencyType: Currency)->String{
        switch currencyType {
        case .EUR:
            return "€"
        case .RUB:
            return "₽"
        case .USD:
            return "$"
            
        }
    }
    
    func getCountry(currencyType: Currency) -> String{
        switch currencyType{
        case .EUR:
            return "european-union"
        case .RUB:
            return "russia"
        case .USD:
            return "united-states"
        }
    }
    
    func generateURL(endYear:Int, priceInPast: String, currencyType: Currency)-> String{
        return "https://www.statbureau.org/calculate-inflation-price-json?country=\(getCountry(currencyType: currencyType))&format=true&start=2000-10-01&end=\(endYear)-10-01&amount=\(priceInPast)"
    }
    
}
