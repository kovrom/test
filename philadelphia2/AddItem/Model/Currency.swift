//
//  Currency.swift
//  philadelphia2
//
//  Created by Роман Ковайкин on 21.10.2020.
//

import Foundation

enum Currency: String, CaseIterable, Identifiable {
    case RUB
    case USD
    case EUR

    var id: String { self.rawValue }
}

