//
//  NameOfItem.swift
//  philadelphia2
//
//  Created by Роман Ковайкин on 21.10.2020.
//

import SwiftUI

//struct AppView : View {
//    @EnvironmentObject var session: SessionStore
//    @State var query = ""
//
//    var body: some View {
//
//
//        return NavigationView {
//
//            Text("Hello World")
//
//            }
//    }
//}
//
//#if DEBUG
//struct AppView_Previews : PreviewProvider {
//    static var previews: some View {
//        AppView()
//            .environmentObject(SessionStore())
//    }
//}
//#endif


struct NameOfItem: View {

    @State private var name: String = ""

    var body: some View {

        NavigationView{

            VStack(spacing: 30) {
                TextField("Название", text: $name) { (isEditing) in
                    print(isEditing)
                } onCommit: {
                    print("commit")
                }
                .frame(height: 60)
                .padding()


                HStack{
                    Spacer()
                        NavigationLink("Дальше", destination: PriceView(name: name))
                        .frame(width: 100, height: 50)
                        .cornerRadius(16)
                        .background(Color.init(CGColor(red: 1, green: 10/255, blue: 83/255, alpha: 1)))
                        .foregroundColor(.white)
                        .cornerRadius(16)
                        .padding(.trailing, 20)
                }
                Spacer()
            }

            .navigationBarTitle(Text("Добавление товара:"), displayMode: .large)

        }
    }

}


struct NameOfItem_Previews: PreviewProvider {
    static var previews: some View {
        NameOfItem().environmentObject(SessionStore())
    }
}
