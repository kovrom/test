//
//  currentPrice.swift
//  philadelphia2
//
//  Created by Роман Ковайкин on 21.10.2020.
//

import SwiftUI
import Alamofire


struct currentPrice: View {
    let viewModel = AddItemViewModel()
    
    @State private var currentPrice: String = ""
    var priceInPast: String
    var name: String
    @State var data: [String] = []
    @State var currencyType: Currency
    
    func initData()-> [String]{
        getInfaltionPrice(url: viewModel.generateURL(endYear: 2005, priceInPast: priceInPast, currencyType: currencyType), maxSize: 1)
        getInfaltionPrice(url: viewModel.generateURL(endYear: 2010, priceInPast: priceInPast, currencyType: currencyType), maxSize: 2)
        getInfaltionPrice(url: viewModel.generateURL(endYear: 2015, priceInPast: priceInPast, currencyType: currencyType), maxSize: 3)
        getInfaltionPrice(url: viewModel.generateURL(endYear: 2020, priceInPast: priceInPast, currencyType: currencyType), maxSize: 4)
        return data
    }
    
    
    func getInfaltionPrice(url: String, maxSize:Int){
        if data.count <= maxSize{
            AF.request(url).responseJSON{ (response) in
                switch response.result{
                case .success(let JSON):
                    if data.contains(JSON as! String){
                        return
                    }else{
                        data.append(JSON as! String)
                        return
                    }
                case .failure(let error):
                    print(error)
                }
            }.resume()
        }
    }
    
    var body: some View {
        
        VStack(spacing: 30){
            
            TextField(viewModel.getCurrency(currencyType: currencyType), text: $currentPrice) { (isEditing) in
                
            } onCommit: {
                
            }.padding()
            
            Picker("Валюта", selection: $currencyType) {
                Text("$").tag(Currency.USD)
                Text("Руб.").tag(Currency.RUB)
                Text("€").tag(Currency.EUR)
            }
            .pickerStyle(SegmentedPickerStyle())
            .padding(.horizontal)
            
            HStack(){
                Spacer()
                NavigationLink("Пропустить", destination: GraphView(name: name, price: priceInPast, currentPrice: "0", currencyType: currencyType, data: data.count == 4 ? data : initData()))
                    .frame(width: 150, height: 50)
                    .border(Color.white, width: 1)
                    .background(Color.black)
                    .foregroundColor(Color.white)
                    .cornerRadius(16)
                
                
                NavigationLink("Дальше", destination: GraphView(name: name, price: priceInPast, currentPrice: currentPrice, currencyType: currencyType, data: data.count == 4 ? data : initData()))
                    .frame(width: 100, height: 50)
                    .cornerRadius(16)
                    .background(Color.init(CGColor(red: 1, green: 10/255, blue: 83/255, alpha: 1)))
                    .foregroundColor(.white)
                    .cornerRadius(16)
                    .padding(.trailing, 20)
            }
            Spacer()
        }
        .navigationBarTitle(Text("Цена товара в 2020"), displayMode: .automatic)
    }
    
}

struct currentPrice_Previews: PreviewProvider {
    static var previews: some View {
        currentPrice(priceInPast: "2000", name: "jdfvnkd", currencyType: Currency.RUB)
    }
}
