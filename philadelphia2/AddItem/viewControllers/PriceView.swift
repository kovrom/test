//
//  CostView.swift
//  philadelphia2
//
//  Created by Роман Ковайкин on 21.10.2020.
//

import SwiftUI

struct PriceView: View {
    
    var name: String
    var viewModel = AddItemViewModel()
    @State private var price = ""
    @State private var currencyType: Currency = Currency.RUB
    
    
    
    var body: some View {
        VStack(spacing: 30, content: {
            TextField(viewModel.getCurrency(currencyType: currencyType), text: $price) { (isEditing) in
                print(isEditing)
                print(price.contains("[^0-9]+$"))
            } onCommit: {
                print(price.contains("[^0-9]+$"))
            }
            .frame(height: 50)
            .padding()
            
            
            Picker("Валюта", selection: $currencyType) {
                Text("$").tag(Currency.USD)
                Text("Руб.").tag(Currency.RUB)
                Text("€").tag(Currency.EUR)
            }
            .pickerStyle(SegmentedPickerStyle())
            .padding(.horizontal)
            
            HStack{
                Spacer()
                NavigationLink("Дальше", destination: currentPrice(priceInPast: price, name: name, currencyType: currencyType))
                    .frame(width: 100, height: 50)
                    .cornerRadius(16)
                    .background(Color.init(CGColor(red: 1, green: 10/255, blue: 83/255, alpha: 1)))
                    .foregroundColor(.white)
                    .cornerRadius(16)
                    .padding(.trailing, 20)
                
                
                
            }
            Spacer()
        })
        .navigationBarTitle(Text("Стоимость товара"), displayMode: .large)
    }
}

struct CostView_Previews: PreviewProvider {
    static var previews: some View {
        PriceView(name: "BigMac")
    }
}
